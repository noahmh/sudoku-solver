# Sudoku Solver

## Overview
I want to write a python program that takes in a sudoku board and outputs the solution.

## Things to figure out
- [x] What will the user input look like
- [x] How is the sudoku data going to be processed
- [x] What are all of the methods used in solving a sudoku board
- [x] Solving & optimization
- [ ] Error handling to avoid being stuck in solve mode if board or code doesn't work

## Terminology
- Board - The full sudoku board
- Sector - 3x3 sections of the board (or a house according to real sudoku players)
- Row - Horizontal lines across the board
- Column - vertical lines down the board
- Cell - The specific units of data in the board, 81 in total
- Potential (as p)- The possible numbers for a given cell
### <u>Tasks</u>
#### Handling user input
There are 81 different cells in the sudoku board. I do not want to have a user (myself) type out 81 digits in line in order to interact with the program. Ideally, the input would be inside of a pop up window that displays a blank board, then the user can select cells to type inputs. This would require messing around with something like pygame. That might take a bit of research and effort to pull off, so i want an easy work around for the initial development.
##### solution (for now)
Hard code inputs for testing during development, then look into converting the program into some ui that has a blank sudoku board and displays the solved puzzle afterwards.

---

#### Handling the board data
The board is large and there are quite a few different ways the data needs to be arranged in order to figure out different cells, such as checking all the values in a row or within a sector. A couple ways I've thought of handling this data are these:

1. Make a board a dictionary with a key for every cell.
The naming convention for cells would be something like "r3c6" to denote the cell in row 3, column six. The value for the cell would be the cell's potential, and maybe data about the sector.

2. Make a board dictionary with rows and cells as keys.
This dictionary would access values with something like "sudokuBoard.r4.c6" which would hold the value of the cell.

3. Make the board a matrix.
A matrix is the most visually appealing way to go about this, as it would look pretty similar to a regular sudoku board. The biggest difficulty I could think of with this method is accessing sector data. Row and column data would be easy enough, but sector data throws a curveball as you would have to access only a select couple of indexes from select rows, dependent on the cell you look at.

##### solution
Overall, I think a key for every cell is the easiest approach. Referencing a cell to get any of its info would be a string slicing of the name, followed by making an array for each of the cells that match. For the sector, I could put that data inside a cell on its creation or run a function to check only cells within a certain boundary for the range.

edit: it might be a good idea to add column and row values for each cell. I think its easier to get all the necessary cells that way instead of iterating through all 81 strings, slicing, and checking those slices.

---

#### Handling solve methods for the board
Sudoku boards have a wide range in level of difficulty, which is not always determined by the number of prefilled squares. Most common boards can be solved by just continually checking the rows, columns, and sectors to determine what numbers can go in a cell. More difficult puzzles require a few techniques go beyond this. I am not very well-versed in these methods even though I've used them without really knowing them. Here is a list of the techniques with reference (there are more than this, but these are the ones I think are relevant to solving with my program):

There are a lot of techniques to cover. For the program, ill start with only basic techniques and then add on the advanced ones. Now, I want to pseudo-pseudo code the functions to get a general scope of the process.

##### Basics
- [Open Singles](https://www.learn-sudoku.com/open-singles.html)
Run a check thru the values of the row/column/sector (RCS) for a cell. If all other cells have a value, set cell to missing value
- [Lone Singles](https://www.learn-sudoku.com/lone-singles.html)
If a cell only has 1 value left for its potential, set the cell to that value
- [Hidden Singles](https://www.learn-sudoku.com/hidden-singles.html)
Check the potential of the RCS for a cell. If the cell has a unique potential, set to that value
- [Naked Pairs](https://www.learn-sudoku.com/naked-pairs.html)
If 2 cells in a RCS have only the same 2 values in their potential, remove those values from the potential of other cells in the RCS.
- [Naked Triples & Quads](https://www.learn-sudoku.com/naked-triplets.html)
Could be the same process as pairs, except with 3-4 values. This might be tricky because it can happen that 3 cells all have 2 potentials but they are "linked" together in a way if 1 changes, the rest changes. (come back to this)
- [Omission](https://www.learn-sudoku.com/omission.html)
If a R/C has potential for a value in only 1 sector, remove potential for that value from other cells in that sector. The inverse is possible too. If a sector only has potential in 1 R/C, remove value from potential in the cells of that R/C
- [Hidden Pairs](https://www.learn-sudoku.com/hidden-pairs.html)
If two cells are the only cells in a RCS to contain some 2 values, remove all other values from their potential.
- [Hidden Triples & Quads](https://www.learn-sudoku.com/hidden-triplets.html)
(come back to this)
##### Advanced
- [X Wing](https://www.learn-sudoku.com/x-wing.html)
- [Swordfish](https://www.learn-sudoku.com/swordfish.html)
- [XY Wing](https://www.learn-sudoku.com/xy-wing.html)

---

#### Handling Solving & Optimization

##### General Solving Process
- Set the potentials for every cell on the board
Do a broad check for each row, then each column, and sector. This should check every cell to some potential.
- Placing values
When a value gets set from any check, set its potential to nothing and remove that value from the potentials of its RCS
- After broad checks
This might be unnecessary. At first I was thinking that I should do initial checks before checking out more complicated techniques (pairs etc..) but It might be better to check for those during a RCS check.

##### Complications
During the first checks when potentials are getting set, should I stop the program from branching off to do checks on its RCS?
My plan is that after a cell gets placed, the program would check its RCS, since the new value gives more info to those cells.  During the initial period, this would lead to many cell checks. I'm not sure if this is going to be an issue so I guess I'll figure that out.

##### Optimization
If I simply start at r1c1, run all of the checks, then move to r1c2 etc.., its going to be extremely slow. If a person solved a puzzle like this it would take ages. It makes more sense if, after swapping, you check some cell that is relevant to any new information, like a cell in the same column.

I'm trying to figure out when the program should care about changes in the potential of a cell. I want the program to run checks against a cell's RCS when it places a value, but its necessary to do this when a cell's potential changes as well. I don't want to have to check a cell's RCS every time its potential changes though, since this would lead to a lot of unnecessary checks. I think it'll be a better practice to trigger a check on RCS when a more advanced function (pairs etc...) and finds a positive match.
