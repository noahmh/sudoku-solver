## 08/23/2023

It has been a few days since I started working on this project. Originally, I was going to impliment a bunch of solving methods in a very organic manner but I am realizing that is a silly way to do this. Instead, the solve method will be geared towards a plug and play method where I will still use general deduction to get the board to a good state, then randomly test values and see what works. I might change my mind in the future though. The inspiration for this project was my own personal struggle with finding complicated patterns such as sword-fish and XY wings, so I wanted to make a program that could detect those for me and act like an automated form of hinting towards a solution.

For now, I still need to impliment the basic checks which I will work on now.

An issue that I am running into deals with calling attributes of a class instance. I wanted to have a method on the class which would take in a value and a cell, then set the value of the cell to that value. Seems simple enough but I can't seem to figure out how to pass that into the function appropriately. When I try to use dot notation to call the attribute, python doesn't recognize the variable passed in from the function. I will update in a future log when I figure out why this is and how to address it.

Also, for clarity, I want to set some definitions of the functions I have set up:
- get : get data
- show : get data and print to the console
- check : get data then modulate it

===

First run completed! The solver worked on today's easy puzzle from sudoku.com. I already know that there are a bunch of cases where this solver will not work, but I am still giddy watching it blast out a little sudoku puzzle so quickly.
