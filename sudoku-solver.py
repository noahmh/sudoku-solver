import math



def set_sector(row, col):
    sectors = [["A", "B", "C"], ["D", "E", "F"], ["G", "H", "I"]]
    row = math.ceil(row/3)
    col = math.ceil(col/3)
    return sectors[row -1][col -1]


def name_constructor(cell_data):
    return f"r{cell_data['row']}c{cell_data['col']}"


class Board:

    def __init__(self, board_state):
        for cell, value in board_state.items():
            row = cell[1]
            column = cell[3]
            sector = set_sector(int(row), int(column))
            potential = [1,2,3,4,5,6,7,8,9]
            if value != 0:
                potential = []

            cell_info = {
                "val": value,
                "p" : potential,
                "row": row,
                "col": column,
                "sect": sector,
            }
            self.__setattr__(cell, cell_info)

    def get_cell(self, cell):
        return self.__dict__[cell]

    def get_row(self, row):
        cells = []
        for cell, data in self.__dict__.items():
            if data["row"] == str(row):
                cells.append(data)
        return cells

    def get_column(self, col):
        cells = []
        for cell, data in self.__dict__.items():
            if data["col"] == str(col):
                cells.append(data)
        return cells

    def get_sector(self, sect):
        cells = []
        for cell, data in self.__dict__.items():
            if data["sect"] == sect:
                cells.append(data)
        return cells

    def get_rcs_p(self, cell):
        cell = self.__dict__[cell]
        r = cell["row"]
        c = cell["col"]
        s = cell["sect"]
        potentials = {
            "row" : [],
            "col" : [],
            "sect" : [],
        }

        r_data = self.get_row(r)
        c_data = self.get_column(c)
        s_data = self.get_sector(s)

        for r_cell in r_data:
            if r_cell == cell:
                continue
            potentials["row"] += r_cell["p"]
        for c_cell in c_data:
            if c_cell == cell:
                continue
            potentials["col"] += c_cell["p"]
        for s_cell in s_data:
            if s_cell == cell:
                continue
            potentials["sect"] += s_cell["p"]

        potentials["row"] = set(potentials["row"])
        potentials["col"] = set(potentials["col"])
        potentials["sect"] = set(potentials["sect"])

        return potentials

    def show_board(self):
        vals = ""
        for num in range(1,10):
            row_data = self.get_row(num)
            for cell in row_data:
                vals += f"{str(cell['val'])}, "
        vals = vals[:-2]
        q=lambda x,y:x+y+x+y+x
        r=lambda a,b,c,d,e:a+q(q(b*3,c),d)+e+"\n"
        return ((r(*"╔═╤╦╗")+q(q("║ %d │ %d │ %d "*3+"║\n",r(*"╟─┼╫╢")),r(*"╠═╪╬╣"))+r(*"╚═╧╩╝"))%eval(vals)).replace(*"0 ")

    def show_all_data(self):
        for num in range(1,10):
            print(f"-----ROW: {num}-----")
        return "SUDOKU"

    def get_all_values(self):
        vals = []
        for num in range(1,10):
            row = self.get_row(num)
            for entry in row:
                vals.append(entry["val"])
        return vals


def solver(board, cell):
    cell_name = cell
    cell = getattr(board, cell)
    if cell["val"] > 0:
        return
    row = board.get_row(cell["row"])
    col = board.get_column(cell["col"])
    sect = board.get_sector(cell["sect"])
    potentials = board.get_rcs_p(cell_name)

    valued = False
    vals = []
    r_potential = potentials["row"]
    c_potential = potentials["col"]
    s_potential = potentials["sect"]

    ## This handles the initial setting of potentials based on values of related cells
    for r_data in row:
        val = r_data["val"]
        if val > 0 and val not in vals:
            vals.append(val)
    for c_data in col:
        val = c_data["val"]
        if val > 0 and val not in vals:
            vals.append(val)
    for s_data in sect:
        val = s_data["val"]
        if val > 0 and val not in vals:
            vals.append(val)
    for item in vals:
        if item in cell["p"]:
            cell["p"].remove(item)

    # This will check the cell's potential against its RCS to see if it contains a unique value
    for cell_p in cell["p"]:
        if cell_p not in r_potential:
            cell["val"] = cell_p
            cell["p"] = []
            setattr(board, cell_name, cell)
            valued = True
            print("RRR Valued placed at", cell_name, cell["val"])
        elif cell_p not in c_potential:
            cell["val"] = cell_p
            cell["p"] = []
            setattr(board, cell_name, cell)
            valued = True
            print("CCC Valued placed at", cell_name, cell["val"])
        elif cell_p not in s_potential:
            cell["val"] = cell_p
            cell["p"] = []
            setattr(board, cell_name, cell)
            valued = True
            print("SSS Valued placed at", cell_name, cell["val"])

    ## This will adjust the value of a cell if it only has 1 value for its potential
    if len(cell["p"]) == 1:
        cell["val"] = cell["p"][0]
        cell["p"] = []
        setattr(board, cell_name, cell)
        valued = True
        print("XXX Valued placed at", cell_name, cell["val"])

    ## I want to re-run the solver on every related cell if a value is found by any method
    if valued:
        related_cells = []
        for r_cell in row:
            r_name = name_constructor(r_cell)
            if r_name not in related_cells:
                related_cells.append(r_name)
        for c_cell in col:
            c_name = name_constructor(c_cell)
            if c_name not in related_cells:
                related_cells.append(c_name)
        for s_cell in sect:
            s_name = name_constructor(s_cell)
            if s_name not in related_cells:
                related_cells.append(s_name)
        for related_cell in related_cells:
            solver(board, related_cell)
    return getattr(board, cell_name)


def input_handler():

    global user_input
    user_input = {}

    board = [4,0,8,5,7,1,9,0,6,2,7,6,8,0,9,3,0,5,0,1,0,0,0,0,4,0,7,0,0,0,4,0,8,0,6,1,0,2,0,0,0,0,0,0,8,6,0,0,7,3,0,0,9,0,0,0,0,0,0,3,0,7,0,8,0,3,0,0,0,0,0,9,0,9,0,1,8,0,0,0,0]
    counter = 0

    for num_1 in range(1,10):
        for num_2 in range(1,10):
            user_input[f"r{num_1}c{num_2}"] = board[counter]
            counter += 1
    # This section is the actual input, but I dont want to type into the input for testing so it stays commented for now.

    # user_input = {}
    # for num in range(1,10):
    #     print(f"Please enter the values for Row: {num}")
    #     user_row = input()
    #     user_row = list(enumerate(user_row.split(",")))
    #     for user_cell in user_row:
    #         user_input[f"r{num}c{user_cell[0]+1}"] = int(user_cell[1])


input_handler()

user_board = Board(user_input)
board_dict = user_board.__dict__


print(user_board.show_board())
for cell_name in board_dict.keys():
    solver(user_board, cell_name)
print(user_board.show_board())
